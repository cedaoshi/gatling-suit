package org.xinhe.perf.gatling.data.collector.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Count</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Count {
    private int total;
    private int ok;
    private int ko;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOk() {
        return ok;
    }

    public void setOk(int ok) {
        this.ok = ok;
    }

    public int getKo() {
        return ko;
    }

    public void setKo(int ko) {
        this.ko = ko;
    }

    @Override
    public String toString() {
        return "Count{" +
                "total=" + total +
                ", ok=" + ok +
                ", ko=" + ko +
                '}';
    }
}

