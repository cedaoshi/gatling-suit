package org.xinhe.perf.gatling.data.collector.monitor;

/**
 * <p>Title: CpuData</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class CpuData {
    private int load;
    private int user;
    private int system;

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getSystem() {
        return system;
    }

    public void setSystem(int system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "CpuData{" +
                "load=" + load +
                ", user=" + user +
                ", system=" + system +
                '}';
    }
}
