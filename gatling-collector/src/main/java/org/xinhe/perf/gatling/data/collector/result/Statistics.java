package org.xinhe.perf.gatling.data.collector.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Statistics</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Statistics {
    private String name;
    private Count numberOfRequests;
    private Count minResponseTime;
    private Count maxResponseTime;
    private Count meanResponseTime;
    private Count standardDeviation;
    private Count percentiles1;
    private Count percentiles2;
    private Count percentiles3;
    private Count percentiles4;
    private Percentage group1;
    private Percentage group2;
    private Percentage group3;
    private Percentage group4;
    private Count meanNumberOfRequestsPerSecond;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Count getNumberOfRequests() {
        return numberOfRequests;
    }

    public void setNumberOfRequests(Count numberOfRequests) {
        this.numberOfRequests = numberOfRequests;
    }

    public Count getMinResponseTime() {
        return minResponseTime;
    }

    public void setMinResponseTime(Count minResponseTime) {
        this.minResponseTime = minResponseTime;
    }

    public Count getMaxResponseTime() {
        return maxResponseTime;
    }

    public void setMaxResponseTime(Count maxResponseTime) {
        this.maxResponseTime = maxResponseTime;
    }

    public Count getMeanResponseTime() {
        return meanResponseTime;
    }

    public void setMeanResponseTime(Count meanResponseTime) {
        this.meanResponseTime = meanResponseTime;
    }

    public Count getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(Count standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public Count getPercentiles1() {
        return percentiles1;
    }

    public void setPercentiles1(Count percentiles1) {
        this.percentiles1 = percentiles1;
    }

    public Count getPercentiles2() {
        return percentiles2;
    }

    public void setPercentiles2(Count percentiles2) {
        this.percentiles2 = percentiles2;
    }

    public Count getPercentiles3() {
        return percentiles3;
    }

    public void setPercentiles3(Count percentiles3) {
        this.percentiles3 = percentiles3;
    }

    public Count getPercentiles4() {
        return percentiles4;
    }

    public void setPercentiles4(Count percentiles4) {
        this.percentiles4 = percentiles4;
    }

    public Percentage getGroup1() {
        return group1;
    }

    public void setGroup1(Percentage group1) {
        this.group1 = group1;
    }

    public Percentage getGroup2() {
        return group2;
    }

    public void setGroup2(Percentage group2) {
        this.group2 = group2;
    }

    public Percentage getGroup3() {
        return group3;
    }

    public void setGroup3(Percentage group3) {
        this.group3 = group3;
    }

    public Percentage getGroup4() {
        return group4;
    }

    public void setGroup4(Percentage group4) {
        this.group4 = group4;
    }

    public Count getMeanNumberOfRequestsPerSecond() {
        return meanNumberOfRequestsPerSecond;
    }

    public void setMeanNumberOfRequestsPerSecond(Count meanNumberOfRequestsPerSecond) {
        this.meanNumberOfRequestsPerSecond = meanNumberOfRequestsPerSecond;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "name='" + name + '\'' +
                ", numberOfRequests=" + numberOfRequests +
                ", minResponseTime=" + minResponseTime +
                ", maxResponseTime=" + maxResponseTime +
                ", meanResponseTime=" + meanResponseTime +
                ", standardDeviation=" + standardDeviation +
                ", percentiles1=" + percentiles1 +
                ", percentiles2=" + percentiles2 +
                ", percentiles3=" + percentiles3 +
                ", percentiles4=" + percentiles4 +
                ", group1=" + group1 +
                ", group2=" + group2 +
                ", group3=" + group3 +
                ", group4=" + group4 +
                ", meanNumberOfRequestsPerSecond=" + meanNumberOfRequestsPerSecond +
                '}';
    }
}
