package org.xinhe.perf.gatling.data.collector.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Percentage</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Percentage {
    private String name;
    private int count;
    private int percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "Percentage{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", percentage=" + percentage +
                '}';
    }
}