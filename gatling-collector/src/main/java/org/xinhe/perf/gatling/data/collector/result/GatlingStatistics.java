package org.xinhe.perf.gatling.data.collector.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

/**
 * <p>Title: GatlingStatistics</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GatlingStatistics {

    private String type;
    private String name;
    private String path;
    private String pathFormatted;
    private Statistics stats;
    private Map<String, ApiTestResult> contents;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathFormatted() {
        return pathFormatted;
    }

    public void setPathFormatted(String pathFormatted) {
        this.pathFormatted = pathFormatted;
    }

    public Statistics getStats() {
        return stats;
    }

    public void setStats(Statistics stats) {
        this.stats = stats;
    }

    public Map<String, ApiTestResult> getContents() {
        return contents;
    }

    public void setContents(Map<String, ApiTestResult> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "GatlingStatistics{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", pathFormatted='" + pathFormatted + '\'' +
                ", stats=" + stats +
                ", contents=" + contents +
                '}';
    }
}
