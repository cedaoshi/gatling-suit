package test.xinhe.perf.gatling.test

/**
  * <p>Title: ComputeApi10050</p>
  * <p>Date: 2018/9/30 </p>
  * <p>Description: </p>
  *
  * @author sunxinhe
  */
class ComputeApi10050 extends ComputeApi {
  override def userAmount(): Int = 1000
}
