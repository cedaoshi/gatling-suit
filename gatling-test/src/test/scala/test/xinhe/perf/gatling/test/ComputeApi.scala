package test.xinhe.perf.gatling.test

import io.gatling.core.Predef.Simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, _}
import io.gatling.http.request.builder.HttpRequestBuilder

import scala.concurrent.duration.{Duration, FiniteDuration}


/**
  * <p>Title: ComputeApi</p>
  * <p>Date: 2018/9/30 </p>
  * <p>Description: </p>
  *
  * @author sunxinhe
  */
abstract class ComputeApi extends Simulation {


  val scn = scenario("computeApiDebug").repeat(300) {
    exec(
      query()
    )
//      .pause(Duration.apply(1, TimeUnit.MILLISECONDS))
  }

  setUp(
    scn.inject(
      rampUsers(userAmount()) over (FiniteDuration.apply(1, "seconds"))
    )
  ).maxDuration(FiniteDuration.apply(5, "minutes"))


  //region Request Prepare
  def userAmount(): Int

  def queryName(): String = {
    "Compute-Api-".concat(userAmount().toString)
  }

  def query(): HttpRequestBuilder = {

    http(queryName())
      .get("http://localhost/")

//      .header("Content-Type", "application/json")

      .check(status.is(200))
  }

  //endregion
}
