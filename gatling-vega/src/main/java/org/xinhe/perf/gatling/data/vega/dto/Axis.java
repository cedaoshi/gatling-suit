package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Axis</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Axis {

    private String field;
    private String type;
    private String stack;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }

    @Override
    public String toString() {
        return "Axis{" +
                "field='" + field + '\'' +
                ", type='" + type + '\'' +
                ", stack='" + stack + '\'' +
                '}';
    }
}
