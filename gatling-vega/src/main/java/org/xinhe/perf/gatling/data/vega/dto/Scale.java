package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Scale</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Scale {

    private String y;

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Scale{" +
                "y='" + y + '\'' +
                '}';
    }
}
