package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Resolve</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Resolve {

    private Scale scale;

    public Scale getScale() {
        return scale;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Resolve{" +
                "scale=" + scale +
                '}';
    }
}
