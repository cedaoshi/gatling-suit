package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * <p>Title: Vconcat</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vconcat {

    private int width;
    private Resolve resolve;
    private Data data;
    private String mark;
    private Encoding encoding;
    private List<Layer> layer;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Resolve getResolve() {
        return resolve;
    }

    public void setResolve(Resolve resolve) {
        this.resolve = resolve;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Encoding getEncoding() {
        return encoding;
    }

    public void setEncoding(Encoding encoding) {
        this.encoding = encoding;
    }

    public List<Layer> getLayer() {
        return layer;
    }

    public void setLayer(List<Layer> layer) {
        this.layer = layer;
    }

    @Override
    public String toString() {
        return "Vconcat{" +
                "width=" + width +
                ", resolve=" + resolve +
                ", data=" + data +
                ", mark='" + mark + '\'' +
                ", encoding=" + encoding +
                ", layer=" + layer +
                '}';
    }
}
