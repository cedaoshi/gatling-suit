package org.xinhe.perf.gatling.data.vega;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.xinhe.perf.gatling.data.vega.convertor.TestingResultVegaConvertor;
import org.xinhe.perf.gatling.data.vega.dto.Vconcat;
import org.xinhe.perf.gatling.data.vega.dto.VegaLiteReport;
import org.xinhe.perf.gatling.data.collector.monitor.CpuData;
import org.xinhe.perf.gatling.data.collector.result.GatlingStatistics;
import org.xinhe.perf.gatling.data.vega.convertor.CpuDataVegaConvertor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Title: Test</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class Test {


    public static void main(String[] args) throws IOException {
        String basePath = "/data/workspace/tmp/compare/zuul-filter-tomcat";

        String vegaLiteReportStr = buildVegaReport(basePath);

        System.out.println(vegaLiteReportStr);
    }

    private static String buildVegaReport(String basePath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        String jsonStr = getJsonStr();

        List<GatlingStatistics> gatlingStatisticsList = objectMapper.readValue(new File(basePath + "/perf-qps.json"), new TypeReference<ArrayList<GatlingStatistics>>() {
        });
        VegaLiteReport vegaLiteReport = TestingResultVegaConvertor.convert(gatlingStatisticsList);


        List<CpuData> cpuDataList = objectMapper.readValue(new File(basePath + "/perf-cpu.json"), new TypeReference<ArrayList<CpuData>>() {
        });
        Vconcat cpuVconcat = CpuDataVegaConvertor.convert(cpuDataList);
        vegaLiteReport.getVconcat().add(cpuVconcat);

        objectMapper.writeValue(new File(basePath + "/perf-vega.json"),vegaLiteReport);
        String vegaLiteReportJson = objectMapper.writeValueAsString(vegaLiteReport);
        return vegaLiteReportJson;
    }

    private static String getJsonStr() {
        return "[{\n" +
                "    \"type\": \"GROUP\",\n" +
                "    \"name\": \"Global Information\",\n" +
                "    \"path\": \"aaa\",\n" +
                "    \"pathFormatted\": \"group_missing-name-b06d1\",\n" +
                "    \"stats\": {\n" +
                "        \"name\": \"Global Information\",\n" +
                "        \"numberOfRequests\": {\n" +
                "            \"total\": 1500,\n" +
                "            \"ok\": 1500,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"minResponseTime\": {\n" +
                "            \"total\": 8,\n" +
                "            \"ok\": 8,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"maxResponseTime\": {\n" +
                "            \"total\": 562,\n" +
                "            \"ok\": 562,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"meanResponseTime\": {\n" +
                "            \"total\": 137,\n" +
                "            \"ok\": 137,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"standardDeviation\": {\n" +
                "            \"total\": 89,\n" +
                "            \"ok\": 89,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"percentiles1\": {\n" +
                "            \"total\": 127,\n" +
                "            \"ok\": 127,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"percentiles2\": {\n" +
                "            \"total\": 187,\n" +
                "            \"ok\": 187,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"percentiles3\": {\n" +
                "            \"total\": 293,\n" +
                "            \"ok\": 293,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"percentiles4\": {\n" +
                "            \"total\": 445,\n" +
                "            \"ok\": 445,\n" +
                "            \"ko\": 0\n" +
                "        },\n" +
                "        \"group1\": {\n" +
                "            \"name\": \"t < 800 ms\",\n" +
                "            \"count\": 1500,\n" +
                "            \"percentage\": 100\n" +
                "        },\n" +
                "        \"group2\": {\n" +
                "            \"name\": \"800 ms < t < 1200 ms\",\n" +
                "            \"count\": 0,\n" +
                "            \"percentage\": 0\n" +
                "        },\n" +
                "        \"group3\": {\n" +
                "            \"name\": \"t > 1200 ms\",\n" +
                "            \"count\": 0,\n" +
                "            \"percentage\": 0\n" +
                "        },\n" +
                "        \"group4\": {\n" +
                "            \"name\": \"failed\",\n" +
                "            \"count\": 0,\n" +
                "            \"percentage\": 0\n" +
                "        },\n" +
                "        \"meanNumberOfRequestsPerSecond\": {\n" +
                "            \"total\": 250,\n" +
                "            \"ok\": 250,\n" +
                "            \"ko\": 0\n" +
                "        }\n" +
                "    },\n" +
                "    \"contents\": {\n" +
                "        \"req_compute-api-50-54b7e\": {\n" +
                "            \"type\": \"REQUEST\",\n" +
                "            \"name\": \"Compute-Api-50\",\n" +
                "            \"path\": \"Compute-Api-50\",\n" +
                "            \"pathFormatted\": \"req_compute-api-50-54b7e\",\n" +
                "            \"stats\": {\n" +
                "                \"name\": \"Compute-Api-50\",\n" +
                "                \"numberOfRequests\": {\n" +
                "                    \"total\": 1500,\n" +
                "                    \"ok\": 1500,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"minResponseTime\": {\n" +
                "                    \"total\": 8,\n" +
                "                    \"ok\": 8,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"maxResponseTime\": {\n" +
                "                    \"total\": 562,\n" +
                "                    \"ok\": 562,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"meanResponseTime\": {\n" +
                "                    \"total\": 137,\n" +
                "                    \"ok\": 137,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"standardDeviation\": {\n" +
                "                    \"total\": 89,\n" +
                "                    \"ok\": 89,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"percentiles1\": {\n" +
                "                    \"total\": 127,\n" +
                "                    \"ok\": 127,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"percentiles2\": {\n" +
                "                    \"total\": 187,\n" +
                "                    \"ok\": 187,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"percentiles3\": {\n" +
                "                    \"total\": 293,\n" +
                "                    \"ok\": 293,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"percentiles4\": {\n" +
                "                    \"total\": 445,\n" +
                "                    \"ok\": 445,\n" +
                "                    \"ko\": 0\n" +
                "                },\n" +
                "                \"group1\": {\n" +
                "                    \"name\": \"t < 800 ms\",\n" +
                "                    \"count\": 1500,\n" +
                "                    \"percentage\": 100\n" +
                "                },\n" +
                "                \"group2\": {\n" +
                "                    \"name\": \"800 ms < t < 1200 ms\",\n" +
                "                    \"count\": 0,\n" +
                "                    \"percentage\": 0\n" +
                "                },\n" +
                "                \"group3\": {\n" +
                "                    \"name\": \"t > 1200 ms\",\n" +
                "                    \"count\": 0,\n" +
                "                    \"percentage\": 0\n" +
                "                },\n" +
                "                \"group4\": {\n" +
                "                    \"name\": \"failed\",\n" +
                "                    \"count\": 0,\n" +
                "                    \"percentage\": 0\n" +
                "                },\n" +
                "                \"meanNumberOfRequestsPerSecond\": {\n" +
                "                    \"total\": 250,\n" +
                "                    \"ok\": 250,\n" +
                "                    \"ko\": 0\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}]";
    }
}
