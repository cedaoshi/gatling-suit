package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

/**
 * <p>Title: Data</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    private List<Map<String,Object>> values;

    public List<Map<String, Object>> getValues() {
        return values;
    }

    public void setValues(List<Map<String, Object>> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Data{" +
                "values=" + values +
                '}';
    }
}
