package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * <p>Title: VegaLiteReport</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VegaLiteReport {

    private String $schema;
    private List<Vconcat> vconcat;

    public String get$schema() {
        return $schema;
    }

    public void set$schema(String $schema) {
        this.$schema = $schema;
    }

    public List<Vconcat> getVconcat() {
        return vconcat;
    }

    public void setVconcat(List<Vconcat> vconcat) {
        this.vconcat = vconcat;
    }





    @Override
    public String toString() {
        return "VegaLiteReport{" +
                "$schema='" + $schema + '\'' +
                ", vconcat=" + vconcat +
                '}';
    }
}
