package org.xinhe.perf.gatling.data.vega.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>Title: Encoding</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Encoding {

    private Axis x;
    private Axis y;
    private Color color;

    public Axis getX() {
        return x;
    }

    public void setX(Axis x) {
        this.x = x;
    }

    public Axis getY() {
        return y;
    }

    public void setY(Axis y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Encoding{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                '}';
    }
}
