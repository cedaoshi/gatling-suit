package org.xinhe.perf.gatling.data.vega.convertor;

import org.apache.commons.collections.CollectionUtils;
import org.xinhe.perf.gatling.data.collector.monitor.CpuData;
import org.xinhe.perf.gatling.data.vega.dto.*;

import java.util.*;

/**
 * <p>Title: CpuDataVegaConvertor</p>
 * <p>Date: 2018/9/30 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class CpuDataVegaConvertor {


    public static Vconcat convert(List<CpuData> cpuDatas) {
        List<Map<String, Object>> cpuList = new LinkedList<>();
        List<Map<String, Object>> loadList = new LinkedList<>();
        buildValue(cpuDatas, cpuList, loadList);

        Vconcat vconcat = new Vconcat();
        vconcat.setWidth(600);

        ArrayList<Layer> layers = new ArrayList<>();
        // cpu
        Layer cpuLayer = new Layer();
        cpuLayer.setMark("bar");
        cpuLayer.setEncoding(getCpuEncoding());
        Data cpuData = new Data();
        cpuData.setValues(cpuList);
        cpuLayer.setData(cpuData);
        layers.add(cpuLayer);

        Layer loadLayer = new Layer();
        loadLayer.setMark("line");
        loadLayer.setEncoding(getLoadEncoding());
        Data loadData = new Data();
        loadData.setValues(loadList);
        loadLayer.setData(loadData);
        layers.add(loadLayer);

        Resolve resolve = new Resolve();
        Scale scale = new Scale();
        scale.setY("independent");
        resolve.setScale(scale);
        vconcat.setResolve(resolve);

        vconcat.setLayer(layers);

        return vconcat;
    }

    private static void buildValue(List<CpuData> cpuDatas, List<Map<String, Object>> cpuList, List<Map<String, Object>> loadList) {
        if (CollectionUtils.isNotEmpty(cpuDatas)) {

            for (int i = 0; i < CollectionUtils.size(cpuDatas); i++) {
                CpuData cpuData = cpuDatas.get(i);
                if (cpuData == null) {
                    continue;
                }

                Map<String, Object> cpuUsValue = new LinkedHashMap<>();
                cpuUsValue.put("Time", i);
                cpuUsValue.put("Cpu", cpuData.getUser());
                cpuUsValue.put("title", "CpuUs");
                cpuList.add(cpuUsValue);

                Map<String, Object> cpuSyValue = new LinkedHashMap<>();
                cpuSyValue.put("Time", i);
                cpuSyValue.put("Cpu", cpuData.getSystem());
                cpuSyValue.put("title", "CpuSy");
                cpuList.add(cpuSyValue);

                Map<String, Object> loadValue = new LinkedHashMap<>();
                loadValue.put("Time", i);
                loadValue.put("Load", cpuData.getLoad());
                loadValue.put("title", "Load");
                loadList.add(loadValue);

            }
        }
    }

    private static Encoding getCpuEncoding() {
        Encoding encoding = new Encoding();
        Axis x = new Axis();
        x.setField("Time");
        x.setType("ordinal");
        encoding.setX(x);

        Axis y = new Axis();
        y.setField("Cpu");
        y.setType("quantitative");

        encoding.setY(y);

        Color color = new Color();
        color.setField("title");
        color.setType("nominal");
        encoding.setColor(color);
        return encoding;
    }

    public static Encoding getLoadEncoding() {
        Encoding encoding = new Encoding();
        Axis x = new Axis();
        x.setField("Time");
        x.setType("quantitative");
        encoding.setX(x);

        Axis y = new Axis();
        y.setField("Load");
        y.setType("quantitative");
        encoding.setY(y);

        Color color = new Color();
        color.setField("title");
        color.setType("nominal");
        encoding.setColor(color);
        return encoding;
    }
}
