package org.xinhe.perf.gatling.sql

import io.gatling.core.action.builder.ActionBuilder
import org.xinhe.perf.gatling.sql.protocol.{SqlProtocol, SqlProtocolBuilder}
import org.xinhe.perf.gatling.sql.request.{SqlRequestBuilder, SqlRequestBuilderBase}

object Predef { // someday maybe - implement trait for checks; re: approach from CQL Extension

  val sql = SqlProtocolBuilder

  implicit def sqlBuilderToProtocol(builder: SqlProtocolBuilder): SqlProtocol = builder.build()
  implicit def sqlBuilderActionBuilder(builder: SqlRequestBuilder): ActionBuilder = builder.build()

  def sql(tag: String) = SqlRequestBuilderBase(tag)
}
