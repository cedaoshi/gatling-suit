package test.xinhe.perf.gatling.test.ck

import java.time.{LocalDate, LocalDateTime}

import io.gatling.core.Predef._
import org.xinhe.perf.gatling.sql.Predef.{sql, _}
import org.xinhe.perf.gatling.sql.db.ConnectionPool
import org.xinhe.perf.gatling.util.SftpUtils
import test.xinhe.perf.gatling.test.common.RemoteShellTool

import scala.util.Random


/**
 * @ClassName: WriteJDBCTMP
 * @Date: 2021-4-12
 * @author: sunxinhe
 * @Version: 1.0
 * @Description:
 * 写入测试：单用户单条写入
 *
 */
class WriteJDBCTMP extends Simulation {

  class Host(var ip: String, var port: Int, var userName: String, var password: String)

  val host = new Host("192.168.12.103", 22, "root", "rootroot")

  val conn = ConnectionPool.connection

  val sqlConfig = sql.connection(conn)

  val tableIdentFeeder = for (x <- 0 until 10) yield Map("tableId" -> x)

  val uniqueNumberFeeder = for (x <- 0 until 10) yield Map("unique" -> x)

  val scn =
    scenario("test").repeat(10) {
      feed(uniqueNumberFeeder)
        .exec(sql("All query").selectQuery("select ${unique}"))
    }


  setUp(scn.inject(atOnceUsers(1)))
    .protocols(sqlConfig)

}
