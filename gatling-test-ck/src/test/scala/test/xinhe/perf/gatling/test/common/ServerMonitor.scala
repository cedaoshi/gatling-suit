package test.xinhe.perf.gatling.test.common

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.Date

import scala.util.Random

object ServerMonitor {

  class Host(var ip: String, var port: Int, var userName: String, var password: String)
  def randomInt(start: Int, end: Int): Int = {
    Random.nextInt(end).%(end-start).+(start)
  }

  def randomInt(n: Int): Int = {
    Random.nextInt(n)
  }


  def main(args: Array[String]): Unit = {
    println(randomCode)
  }

  private def randomCode = {
    var code = "cc"
    val chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    for (i <- 1 to 8) {
      code += chars charAt (Random.nextInt(chars.length))
    }
    code
  }

  private def findwakuang = {
    var hostList: List[Host] = List()
    //    hostList :+ new Host("172.20.22.67", 22, "debug", "debug")
    hostList = new Host("192.168.12.103", 22, "root", "rootroot") +: hostList

    for (host <- hostList) {
      top_debug(host)
    }
  }

  private def top_debug(host: Host) = {
    val cmd = "top -b -n 1 | tail -n +6 |  awk '$9>80 || $9==\"%CPU\" {print $0}' "

    println("==== ip : " + host.ip)
    val rms = new RemoteShellTool(host.ip, host.port, host.userName, host.password)
    println(rms.exec("pwd"))

    val result = rms.exec(cmd)
    println(result)
  }

  private def top(ip: String) = {
    val post = "22".toInt
    val userName = "root"
    val password = "rootroot"
    val cmd = "top -b -n 1 | tail -n +6 |  awk '$9>80 || $9==\"%CPU\" {print $0}' "

    println("==== ip : " + ip)
    val rms = new RemoteShellTool(ip, post, userName, password)
    println(rms.exec("pwd"))

    val result = rms.exec(cmd)
    println(result)
  }
}
