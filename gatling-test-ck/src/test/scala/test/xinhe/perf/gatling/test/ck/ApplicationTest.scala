package test.xinhe.perf.gatling.test.ck
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
/**
 * @ClassName: ApplicationTest
 * @Date: 2021-4-9
 * @author: sunxinhe
 * @Version: 1.0
 * @Description:
 * TODO
 *
 */
class ApplicationTest extends Simulation{

  val httpConf = http
    .baseURL("https://xxxx.xxxx") // Here is the root for all relative URLs
    .acceptHeader("application/json, text/plain, */*") // Here are the common headers
    .acceptLanguageHeader("zh-CN,zh;q=0.9,en;q=0.8")
    .acceptEncodingHeader("gzip, deflate, br")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36")

  val headers_10 = Map("Content-Type" -> "application/json") // Note the headers specific to a given request

  val scn = scenario("SigningAdvisor")
    .exec(http("login")
      .post("/advisor-auth/api/auth/xxxx/_actions/loginOrRegisterUser") //test data
      .headers(headers_10)
      .body(StringBody("""{ "phone": "177xxxxxxxx","verificationCode":"7777"}""")).asJSON
      .check(status.is(200))
      .check(jsonPath("$..userId").saveAs("userId"))
      .check(jsonPath("$..token").exists)
      .check(jsonPath("$..token").saveAs("token"))
    )

    // 取出 token 值并修改
    .exec(session => {
      val token = session("token").as[String]
      val token2 = "Bearer " + token
      session.set("token",token2)
    })

    // 打印 session 验证正确性
    .exec { session => println(session)
      session
    }

    .exec(http("advisorSigning")
      .post("/bms-advisor/api/advisors")
      .headers(headers_10)
      .header("Authorization","${token}")
      .body(StringBody("""{"userId": "5a5d9d1f30c49e0008c1c913",
"advisorPhone": "xxxxxx", //  test data
"emailAddress": "Q@123.com",
"advisorName": "xxxx", //test data
"idCardNumber": "xxxxxxx", //test data
"nation": "Han",
"highestDegree": "doctor",
"politicalStatus": "ptgm",
"provinceCompanyId": "59f422cdb2b14f0008a1166d"}""")).asJSON)


  setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))

}
