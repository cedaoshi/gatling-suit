package test.xinhe.perf.gatling.test.ck.write

import test.xinhe.perf.gatling.test.ck.WriteJDBC


class WriteJDBC_1_1 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 1 }
class WriteJDBC_1_10 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 10 }
class WriteJDBC_1_100 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 100 }
class WriteJDBC_1_500 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 500 }
class WriteJDBC_1_1000 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 1000 }
class WriteJDBC_1_2000 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 2000 }
class WriteJDBC_1_3000 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 3000 }
class WriteJDBC_1_4000 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 4000 }
class WriteJDBC_1_5000 extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 5000 }

class WriteJDBC_2_1 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 1 }
class WriteJDBC_2_10 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 10 }
class WriteJDBC_2_100 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 100 }
class WriteJDBC_2_500 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 500 }
class WriteJDBC_2_1000 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 1000 }
class WriteJDBC_2_2000 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 2000 }
class WriteJDBC_2_3000 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 3000 }
class WriteJDBC_2_4000 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 4000 }
class WriteJDBC_2_5000 extends WriteJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 5000 }

class WriteJDBC_3_1 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 1 }
class WriteJDBC_3_10 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 10 }
class WriteJDBC_3_100 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 100 }
class WriteJDBC_3_500 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 500 }
class WriteJDBC_3_1000 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 1000 }
class WriteJDBC_3_2000 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 2000 }
class WriteJDBC_3_3000 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 3000 }
class WriteJDBC_3_4000 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 4000 }
class WriteJDBC_3_5000 extends WriteJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 5000 }

class WriteJDBC_4_1 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 1 }
class WriteJDBC_4_10 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 10 }
class WriteJDBC_4_100 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 100 }
class WriteJDBC_4_500 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 500 }
class WriteJDBC_4_1000 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 1000 }
class WriteJDBC_4_2000 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 2000 }
class WriteJDBC_4_3000 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 3000 }
class WriteJDBC_4_4000 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 4000 }
class WriteJDBC_4_5000 extends WriteJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 5000 }

class WriteJDBC_5_1 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 1 }
class WriteJDBC_5_10 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 10 }
class WriteJDBC_5_100 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 100 }
class WriteJDBC_5_500 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 500 }
class WriteJDBC_5_1000 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 1000 }
class WriteJDBC_5_2000 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 2000 }
class WriteJDBC_5_3000 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 3000 }
class WriteJDBC_5_4000 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 4000 }
class WriteJDBC_5_5000 extends WriteJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 5000 }

class WriteJDBC_6_1 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 1 }
class WriteJDBC_6_10 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 10 }
class WriteJDBC_6_100 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 100 }
class WriteJDBC_6_500 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 500 }
class WriteJDBC_6_1000 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 1000 }
class WriteJDBC_6_2000 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 2000 }
class WriteJDBC_6_3000 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 3000 }
class WriteJDBC_6_4000 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 4000 }
class WriteJDBC_6_5000 extends WriteJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 5000 }

class WriteJDBC_7_1 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 1 }
class WriteJDBC_7_10 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 10 }
class WriteJDBC_7_100 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 100 }
class WriteJDBC_7_500 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 500 }
class WriteJDBC_7_1000 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 1000 }
class WriteJDBC_7_2000 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 2000 }
class WriteJDBC_7_3000 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 3000 }
class WriteJDBC_7_4000 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 4000 }
class WriteJDBC_7_5000 extends WriteJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 5000 }

class WriteJDBC_8_1 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 1 }
class WriteJDBC_8_10 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 10 }
class WriteJDBC_8_100 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 100 }
class WriteJDBC_8_500 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 500 }
class WriteJDBC_8_1000 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 1000 }
class WriteJDBC_8_2000 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 2000 }
class WriteJDBC_8_3000 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 3000 }
class WriteJDBC_8_4000 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 4000 }
class WriteJDBC_8_5000 extends WriteJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 5000 }

class WriteJDBC_9_1 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 1 }
class WriteJDBC_9_10 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 10 }
class WriteJDBC_9_100 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 100 }
class WriteJDBC_9_500 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 500 }
class WriteJDBC_9_1000 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 1000 }
class WriteJDBC_9_2000 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 2000 }
class WriteJDBC_9_3000 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 3000 }
class WriteJDBC_9_4000 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 4000 }
class WriteJDBC_9_5000 extends WriteJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 5000 }


class WriteJDBC_10_1 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 1 }
class WriteJDBC_10_10 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 10 }
class WriteJDBC_10_100 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 100 }
class WriteJDBC_10_500 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 500 }
class WriteJDBC_10_1000 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 1000 }
class WriteJDBC_10_2000 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 2000 }
class WriteJDBC_10_3000 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 3000 }
class WriteJDBC_10_4000 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 4000 }
class WriteJDBC_10_5000 extends WriteJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 5000 }


class WriteJDBCForQuery extends WriteJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 1000;
  override def totalSize(): Int = 10000000;override def queryName(): String = "write_jdbc_for_query"}
