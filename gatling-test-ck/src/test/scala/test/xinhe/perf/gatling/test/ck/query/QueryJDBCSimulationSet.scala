package test.xinhe.perf.gatling.test.ck.query

import test.xinhe.perf.gatling.test.ck.QueryJDBC

class QueryJDBC_1_100 extends QueryJDBC { override def userAmount(): Int = 1; override def batchSize(): Int = 100 }
class QueryJDBC_2_100 extends QueryJDBC { override def userAmount(): Int = 2; override def batchSize(): Int = 100 }
class QueryJDBC_3_100 extends QueryJDBC { override def userAmount(): Int = 3; override def batchSize(): Int = 100 }
class QueryJDBC_4_100 extends QueryJDBC { override def userAmount(): Int = 4; override def batchSize(): Int = 100 }
class QueryJDBC_5_100 extends QueryJDBC { override def userAmount(): Int = 5; override def batchSize(): Int = 100 }
class QueryJDBC_6_100 extends QueryJDBC { override def userAmount(): Int = 6; override def batchSize(): Int = 100 }
class QueryJDBC_7_100 extends QueryJDBC { override def userAmount(): Int = 7; override def batchSize(): Int = 100 }
class QueryJDBC_8_100 extends QueryJDBC { override def userAmount(): Int = 8; override def batchSize(): Int = 100 }
class QueryJDBC_9_100 extends QueryJDBC { override def userAmount(): Int = 9; override def batchSize(): Int = 100 }
class QueryJDBC_10_100 extends QueryJDBC { override def userAmount(): Int = 10; override def batchSize(): Int = 100 }
class QueryJDBC_15_100 extends QueryJDBC { override def userAmount(): Int = 15; override def batchSize(): Int = 100 }
class QueryJDBC_20_100 extends QueryJDBC { override def userAmount(): Int = 20; override def batchSize(): Int = 100 }
class QueryJDBC_25_100 extends QueryJDBC { override def userAmount(): Int = 25; override def batchSize(): Int = 100 }
class QueryJDBC_30_100 extends QueryJDBC { override def userAmount(): Int = 30; override def batchSize(): Int = 100 }
class QueryJDBC_35_100 extends QueryJDBC { override def userAmount(): Int = 35; override def batchSize(): Int = 100 }
class QueryJDBC_40_100 extends QueryJDBC { override def userAmount(): Int = 40; override def batchSize(): Int = 100 }
class QueryJDBC_45_100 extends QueryJDBC { override def userAmount(): Int = 45; override def batchSize(): Int = 100 }
class QueryJDBC_50_100 extends QueryJDBC { override def userAmount(): Int = 50; override def batchSize(): Int = 100 }
class QueryJDBC_55_100 extends QueryJDBC { override def userAmount(): Int = 55; override def batchSize(): Int = 100 }
class QueryJDBC_60_100 extends QueryJDBC { override def userAmount(): Int = 60; override def batchSize(): Int = 100 }
class QueryJDBC_65_100 extends QueryJDBC { override def userAmount(): Int = 65; override def batchSize(): Int = 100 }
class QueryJDBC_70_100 extends QueryJDBC { override def userAmount(): Int = 70; override def batchSize(): Int = 100 }
class QueryJDBC_75_100 extends QueryJDBC { override def userAmount(): Int = 75; override def batchSize(): Int = 100 }
class QueryJDBC_80_100 extends QueryJDBC { override def userAmount(): Int = 80; override def batchSize(): Int = 100 }
class QueryJDBC_85_100 extends QueryJDBC { override def userAmount(): Int = 85; override def batchSize(): Int = 100 }
class QueryJDBC_90_100 extends QueryJDBC { override def userAmount(): Int = 90; override def batchSize(): Int = 100 }
class QueryJDBC_95_100 extends QueryJDBC { override def userAmount(): Int = 95; override def batchSize(): Int = 100 }
class QueryJDBC_100_100 extends QueryJDBC { override def userAmount(): Int = 100; override def batchSize(): Int = 100 }
