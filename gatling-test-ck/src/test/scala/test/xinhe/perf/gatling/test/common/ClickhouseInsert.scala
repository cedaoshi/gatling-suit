package test.xinhe.perf.gatling.test.common

import org.xinhe.perf.gatling.sql.db.ConnectionPool

/**
 * @ClassName: ClickhouseInsert
 * @Date: 2021-4-12
 * @author: sunxinhe
 * @Version: 1.0
 * @Description:
 * TODO
 *
 */
object ClickhouseInsert {

  val conn = ConnectionPool.connection

  def main(args: Array[String]): Unit = {
    var ret = conn.prepareStatement(sqlQuery()).execute()
    println(ret)


  }

  def sqlQuery(): String = {
    //TODO 生成 Insert sql
    var insertSql: StringBuffer = new StringBuffer("INSERT INTO ck_perf_vehicle_1u1r\n(" +
      "`id`,\n  " +
      "`plate_type`,\n  " +
      "`plate_no`,\n  " +
      "`speed`,\n  " +
      "`appear_time`,\n  " +
      "`mark_time`,\n  " +
      "`device_id`,\n  " +
      "`vehicle_image`,\n  " +
      "`scene_image`,\n  " +
      "`vehicle_color`,\n  " +
      "`area_code`,\n  " +
      "`line_no`,\n  " +
      "`pass_time`,\n  " +
      "`plate_color`,\n  " +
      "`plate_describe`,\n  " +
      "`disappear_time`,\n  " +
      "`vehicle_class`\n) \n" +
      "VALUES")

      insertSql.append(
        "(" +
          "1, " +
          "2, " +
          "'EA10001', " +
          "now(), " +
          "now(), " +
          "now(), " +
          "'ABCDEFG', " +
          "'http://www.gitee.com/img/gRHPNFTOxM', " +
          "'http://www.gitee.com/img/gRHPNFTOxM', " +
          "1," +
          "'310018', " +
          "3, " +
          "now(), " +
          "3, " +
          "4, " +
          "now(), " +
          "2" +
          ")"
      )


    println(insertSql)

    insertSql.toString


  }
}
