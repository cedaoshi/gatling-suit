package test.xinhe.perf.gatling.test.ck.mock

import test.xinhe.perf.gatling.test.ck.MockData

class MockData_2021_04_25 extends MockData { override def dateString(): String = "2021-04-25" }
class MockData_2021_04_24 extends MockData { override def dateString(): String = "2021-04-24" }
class MockData_2021_04_23 extends MockData { override def dateString(): String = "2021-04-23" }
class MockData_2021_04_22 extends MockData { override def dateString(): String = "2021-04-22" }
class MockData_2021_04_21 extends MockData { override def dateString(): String = "2021-04-21" }
class MockData_2021_04_20 extends MockData { override def dateString(): String = "2021-04-20" }
class MockData_2021_04_19 extends MockData { override def dateString(): String = "2021-04-19" }
class MockData_2021_04_18 extends MockData { override def dateString(): String = "2021-04-18" }
class MockData_2021_04_17 extends MockData { override def dateString(): String = "2021-04-17" }
class MockData_2021_04_16 extends MockData { override def dateString(): String = "2021-04-16" }
class MockData_2021_04_15 extends MockData { override def dateString(): String = "2021-04-15" }
class MockData_2021_04_14 extends MockData { override def dateString(): String = "2021-04-14" }
class MockData_2021_04_13 extends MockData { override def dateString(): String = "2021-04-13" }
class MockData_2021_04_12 extends MockData { override def dateString(): String = "2021-04-12" }
class MockData_2021_04_11 extends MockData { override def dateString(): String = "2021-04-11" }
class MockData_2021_04_10 extends MockData { override def dateString(): String = "2021-04-10" }

class MockData_2021_01_01 extends MockData { override def dateString(): String = "2021-01-01" }
