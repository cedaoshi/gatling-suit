#!/bin/bash
userAmount=(1 2 3)

batchSize=(10 100)

for i in ${userAmount[@]}; do
    for j in ${batchSize[*]}; do
        echo "userAmount = ${i} ; batchSize = ${j}"
        echo "mvn gatling:execute -Dgatling.simulationClass=test.xinhe.perf.gatling.test.ck.write.WriteJDBC_${i}_${j}"
        mvn gatling:execute -Dgatling.simulationClass=test.xinhe.perf.gatling.test.ck.write.WriteJDBC_${i}_$j
        sleep 3s && pkill -9f java && ps -ef | grep java
        sleep 1s && pkill -9f java && ps -ef | grep java
        sleep 1s && pkill -9f java && ps -ef | grep java
        sleep 1s && pkill -9f java && ps -ef | grep java
        sleep 1s && pkill -9f java && ps -ef | grep java
        sleep 1s && pkill -9f java && ps -ef | grep java
    done
done
echo "end"