# gatling-suit

#### 项目介绍
gatling 工具包，用于辅助gatling性能测试，包括收集cpu信息、测试结果、生成性能图表等。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

- cpu数据 ```测试之前先开始收集数据，测试完成后，停止收集，取回数据```
    - 获取
        ```
        $ vmstat 1 > perf-cpu.txt
        ```
    - 转json
        ```
        #!/bin/bash
        file=$1
        
        var1=`cat $file | grep -v procs | grep -v swpd| awk '{printf"{\"load\":%s ,\"user\":%s ,\"system\":%s},",$1 ,$13 ,$14}'`
        
        echo "[${var1%*,}]"
    
        ```
        
        or
        
        ```
        $ var1=`cat perf-cpu-concurrent-null.txt | grep -v procs | grep -v swpd| awk '{printf"{\"load\":%s ,\"user\":%s ,\"system\":%s},",$1 ,$13 ,$14}'`;echo "[${var1%*,}]"
        ```


- qps数据
    ```
    $ var1=`find . -name "stats.json"| xargs cat `;echo [${var1//"}{"/"},{"}] > perf-qps.json
    ```


#### 其他脚本
- 统计当前连接数
    ```
    $netstat -an|grep 5555|awk '{count[$6]++} END{for (i in count) print(i,count[i])}'
    e.g.
    LISTEN 1
    ESTABLISHED 10
    TIME_WAIT 20
    
    其中 ESTABLISHED 表示当前并发连接数
    ```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)